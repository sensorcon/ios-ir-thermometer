//
//  AppDelegate.h
//  IR Thermometer
//
//  Created by Mark Rudolph on 11/13/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "SensordroneiOSLibrary.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, DroneEventDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property Drone *myDrone;


-(void)showConnectionLostDialog;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
