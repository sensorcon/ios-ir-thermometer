//
//  MainViewController.m
//  IR Thermometer
//
//  Created by Mark Rudolph on 12/28/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize refLabel;
@synthesize tempLabel;

@synthesize ledBar;

@synthesize ledSegment;
@synthesize refSegment;

@synthesize streamer;
@synthesize referenceTemperature;

@synthesize isCentigrade;
@synthesize isColorEnabled;
@synthesize upperThreshold;
@synthesize lowerThreshhold;
@synthesize isRefEnabled;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    referenceTemperature = 0;
    lowerThreshhold = 0;
    upperThreshold = 0;
    isColorEnabled = NO;
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Default should be NO
    isCentigrade = [[NSUserDefaults standardUserDefaults] boolForKey:@"UNIT"];
    
    
    if ([refSegment selectedSegmentIndex] == 2) {
        if (isCentigrade) {
            referenceTemperature = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_C"];
        }
        else {
            referenceTemperature = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_F"];
        }
    }
    
    
    [self updateUnitLabels:isCentigrade];
    
    if ([self connectionCheck]) {
        [self.delegate.myDrone enableIRTemperature];
    }
    else {
        [tempLabel setText:@"OFF"];
    }
    [self updateRefLabel];
    if ([refSegment selectedSegmentIndex] == 0) {
        isRefEnabled = NO;
    }
    else {
        isRefEnabled = YES;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)refSegmentSelected:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    int selectedIndex = [segmentedControl selectedSegmentIndex];
    
    if (selectedIndex == 0) {
        // Trigger a selection of OFF for the LEDs
        [ledSegment setSelectedSegmentIndex:0];
        [self ledSegmentSelected:ledSegment];
        [self updateRefLabel];
        isRefEnabled = NO;
    }
    else if (selectedIndex == 1) {
        if ([self connectionCheck]) {
            if (isCentigrade) {
                referenceTemperature = [self.delegate.myDrone measuredIRTemperatureInCelcius];
            }
            else {
                referenceTemperature = [self.delegate.myDrone measuredIRTemperatureInFahreneit];
            }
            
            [self updateRefLabel];
            isRefEnabled = YES;
        }
        else {
            [refSegment setSelectedSegmentIndex:0];
        }
        
    }
    else if (selectedIndex == 2) {
        if (isCentigrade) {
            referenceTemperature = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_C"];
        }
        else {
            referenceTemperature = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_F"];
        }
        [self updateRefLabel];
        isRefEnabled = YES;
    }
}

- (IBAction)ledSegmentSelected:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    int selectedIndex = [segmentedControl selectedSegmentIndex];
    
    // Change our flag
    if (selectedIndex == 0) {
        isColorEnabled = NO;
        upperThreshold = 0;
        lowerThreshhold = 0;
        [self setBarColot:0];
    } else {
        isColorEnabled = YES;
    }
    // Change our upper/lower thresholds
    if (isCentigrade) {
        if (selectedIndex == 1) {
            upperThreshold = 1;
            lowerThreshhold = -1;
        }
        else if (selectedIndex == 2) {
            upperThreshold = 5;
            lowerThreshhold = -5;
        }
        else if (selectedIndex == 3) {
            upperThreshold = 10;
            lowerThreshhold = -10;
        }
    }
    else {
        if (selectedIndex == 1) {
            upperThreshold = 2;
            lowerThreshhold = -2;
        }
        else if (selectedIndex == 2) {
            upperThreshold = 10;
            lowerThreshhold = -10;
        }
        else if (selectedIndex == 3) {
            upperThreshold = 20;
            lowerThreshhold = -20;
        }
    }
    
}

-(void)setBarColot:(int)rgb {
    if (rgb == 1) {
        [ledBar setImage:[UIImage imageNamed:@"light_red.png"]];
    }
    else if (rgb == 2) {
        [ledBar setImage:[UIImage imageNamed:@"light_green.png"]];
    }
    else if (rgb == 3) {
        [ledBar setImage:[UIImage imageNamed:@"light_blue.png"]];
    }
    else {
        [ledBar setImage:[UIImage imageNamed:@"light_off.png"]];
    }
}

-(BOOL)connectionCheck {
    if ([self.delegate.myDrone.dronePeripheral state] == CBPeripheralStateConnected) {
        return YES;
    }
    else {
        return NO;
    }
}

-(void)doOnIRTemperatureEnabled {
    if (streamer != nil) {
        [streamer invalidate];
        streamer = nil;
    }
    streamer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self.delegate.myDrone selector:@selector(measureIRTemperature) userInfo:nil repeats:YES];
}

-(void)doOnIRTemperatureDisabled {
    if (streamer != nil) {
        [streamer invalidate];
        streamer = nil;
    }
}

-(void)doOnIRTemperatureMeasured {
    NSString *tempReading;
    
    float tempDiff;
    
    // Update our measured value
    if (isCentigrade) {
        tempReading = [NSString stringWithFormat:@"%0.1f °C",[self.delegate.myDrone measuredIRTemperatureInCelcius]];
        tempDiff = [self.delegate.myDrone measuredIRTemperatureInCelcius] - referenceTemperature;
    } else {
        tempReading = [NSString stringWithFormat:@"%0.1f °F",[self.delegate.myDrone measuredIRTemperatureInFahreneit]];
        tempDiff = [self.delegate.myDrone measuredIRTemperatureInFahreneit] - referenceTemperature;
    }
    [tempLabel setText:tempReading];
    if (isRefEnabled && isColorEnabled) {
        // RED
        if (tempDiff >= upperThreshold) {
            [self setBarColot:1];
            [self.delegate.myDrone setLEDs:142 :0 :0];
        }
        //BLUE
        else if (tempDiff <= lowerThreshhold) {
            [self setBarColot:3];
            [self.delegate.myDrone setLEDs:0 :0 :142];
        }
        //GREEN
        else {
            [self setBarColot:2];
            [self.delegate.myDrone setLEDs:0 :142 :0];
        }
    }
    //OFF
    else {
        [self.delegate.myDrone setLEDs:0 :0 :0];
    }
    
    
}

-(void)updateRefLabel {
    // Where are we
    int position = [refSegment selectedSegmentIndex];
    if (position == 0) {
        [refLabel setText:@"Ref: Off"];
    }
    else {
        if (isCentigrade) {
            [refLabel setText:[NSString stringWithFormat:@"Ref: %0.1f °C", referenceTemperature]];
        }
        else {
            [refLabel setText:[NSString stringWithFormat:@"Ref: %0.1f °F", referenceTemperature]];
        }
    }
}

-(float)referenceTempToF:(float) ref {
    return ref*9/5 + 32;
}

-(void)updateUnitLabels:(BOOL)isC {
    if (isC) {
        [ledSegment setTitle:@"+- 1 °C" forSegmentAtIndex:1];
        [ledSegment setTitle:@"+- 5 °C" forSegmentAtIndex:2];
        [ledSegment setTitle:@"+- 10 °C" forSegmentAtIndex:3];
    }
    else {
        [ledSegment setTitle:@"+- 2 °F" forSegmentAtIndex:1];
        [ledSegment setTitle:@"+- 10 °F" forSegmentAtIndex:2];
        [ledSegment setTitle:@"+- 20 °F" forSegmentAtIndex:3];
    }
}

-(void) doOnConnectionLost {
    [self.bleStatusIcon showDisconnected];
    [self.delegate showConnectionLostDialog];
    if (streamer != nil) {
        [streamer invalidate];
        streamer = nil;
        [tempLabel setText:@"OFF"];
    }
}
@end
