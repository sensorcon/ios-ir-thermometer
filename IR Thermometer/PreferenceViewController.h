//
//  PreferenceViewController.h
//
//  Created by Mark Rudolph on 12/27/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"

@interface PreferenceViewController : BaseViewController <UIPickerViewDelegate>

@property float storedReference;
@property (weak, nonatomic) IBOutlet UISegmentedControl *unitSegment;
@property (weak, nonatomic) IBOutlet UIPickerView *refPicker;

@property BOOL isCentigrade;
- (IBAction)selectedUnit:(id)sender;

@end
