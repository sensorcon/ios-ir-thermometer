//
//  PreferenceViewController.m
//  Virtual CO Inspector
//
//  Created by Mark Rudolph on 12/27/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "PreferenceViewController.h"

@interface PreferenceViewController ()

@end

@implementation PreferenceViewController

@synthesize storedReference;
@synthesize isCentigrade;
@synthesize refPicker;
@synthesize unitSegment;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // Get our stored stuff
    isCentigrade = [[NSUserDefaults standardUserDefaults] boolForKey:@"UNIT"];
    
    
    if (isCentigrade) {
        [unitSegment setSelectedSegmentIndex:1];
        storedReference = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_C"];
    } else {
        [unitSegment setSelectedSegmentIndex:0];
        storedReference = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_F"];
    }
    
    [refPicker reloadAllComponents];
    int storedRow = storedReference + 40;
    [refPicker selectRow:storedRow inComponent:0 animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (isCentigrade) {
        return 161;
    }
    else {
        return 289;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    int rowVal = row - 40; // C
    
    return [NSString stringWithFormat:@"%d", rowVal];
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // When we select a row, store
    int tempToStore = row - 40;
    if (isCentigrade) {
        [[NSUserDefaults standardUserDefaults] setFloat:tempToStore forKey:@"REF_C"];
        storedReference = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_C"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setFloat:tempToStore forKey:@"REF_F"];
        storedReference = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_F"];
    }
    
}

-(float)tempToF:(float) temp {
    return temp*9/5 + 32;
}

-(float)tempToC:(float) temp {
    return (temp - 32) * 5 / 9;
}

- (IBAction)selectedUnit:(id)sender {
    NSLog(@"Stored temp is %.1f", storedReference);
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    int selectedIndex = [segmentedControl selectedSegmentIndex];
    if (selectedIndex == 1) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UNIT"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"UNIT"];
    }
    isCentigrade = [[NSUserDefaults standardUserDefaults] boolForKey:@"UNIT"];
    if (isCentigrade) {
        storedReference = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_C"];
    }
    else {
        storedReference = [[NSUserDefaults standardUserDefaults] floatForKey:@"REF_F"];
    }
    [refPicker reloadAllComponents];
    int storedRow = storedReference + 40;
    [refPicker selectRow:storedRow inComponent:0 animated:YES];
    
}
@end
