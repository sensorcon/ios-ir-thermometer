//
//  InfoViewController.m
//  Oxidizing Gas Sensor Monitor
//
//  Created by Mark Rudolph on 11/14/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController

@synthesize infoView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *localFilePath = [[NSBundle mainBundle] pathForResource:@"info" ofType:@"html"] ;
    NSURLRequest *localRequest = [NSURLRequest requestWithURL:
                                  [NSURL fileURLWithPath:localFilePath]] ;
    NSLog(@"%@", localFilePath);
    [infoView loadRequest:localRequest];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"Loading...");
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"Loaded!");
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"Error loading!");
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

-(void) doOnConnectionLost {
    [self.bleStatusIcon showDisconnected];
    [self.delegate showConnectionLostDialog];    
}
@end
