//
//  MainViewController.h
//  IR Thermometer
//
//  Created by Mark Rudolph on 12/28/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"

@interface MainViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *refSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *ledSegment;

@property (weak, nonatomic) IBOutlet UIImageView *ledBar;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *refLabel;

@property NSTimer *streamer;

@property float referenceTemperature;

@property float upperThreshold;
@property float lowerThreshhold;
@property BOOL isColorEnabled;
@property BOOL isRefEnabled;
@property BOOL isCentigrade;

- (IBAction)refSegmentSelected:(id)sender;
- (IBAction)ledSegmentSelected:(id)sender;

@end
